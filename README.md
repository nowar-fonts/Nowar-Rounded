# 有爱圆体

## 字形和变体

* 标准变体：简体中文、繁体中文分别采用中国大陆、台湾字形
  * Bliz — 其他语言采用中国大陆字形（和《魔兽世界》原始设定相同）
  * Neut — 其他语言采用传统字形
* 传统变体：
  * CL — 各语言均采用传统字形
* 特性变体：
  * Bliz,OSF — 在 Bliz 变体的基础上启用不齐线数字
  * Bliz,RP — 在 Bliz 变体的基础上将 “丶” 映射到 “·”，以方便 RP 玩家
  * Bliz,SC — 在 Bliz 变体的基础上启用小型大写字母
* 伪简体：将繁体字重映射到简化字字形（只替换繁体中文用到的字体文件）
  * PSimp — 聊天字体保持正常映射
  * PSimpChat — 聊天字体也是伪简体

## 字重

![Preview for weights](weight.png)

## 下载链接

| 300 | 400 | 500 | 700 |
| --- | --- | --- | --- |
| [Bliz-300](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz-300-0.3.0.7z) | [Bliz-400](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz-400-0.3.0.7z) | [Bliz-500](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz-500-0.3.0.7z) | [Bliz-700](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz-700-0.3.0.7z) |
| [Neut-300](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Neut-300-0.3.0.7z) | [Neut-400](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Neut-400-0.3.0.7z) | [Neut-500](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Neut-500-0.3.0.7z) | [Neut-700](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Neut-700-0.3.0.7z) |
| [CL-300](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-CL-300-0.3.0.7z) | [CL-400](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-CL-400-0.3.0.7z) | [CL-500](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-CL-500-0.3.0.7z) | [CL-700](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-CL-700-0.3.0.7z) |
| [Bliz,OSF-300](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz,OSF-300-0.3.0.7z) | [Bliz,OSF-400](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz,OSF-400-0.3.0.7z) | [Bliz,OSF-500](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz,OSF-500-0.3.0.7z) | [Bliz,OSF-700](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz,OSF-700-0.3.0.7z) |
| [Bliz,RP-300](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz,RP-300-0.3.0.7z) | [Bliz,RP-400](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz,RP-400-0.3.0.7z) | [Bliz,RP-500](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz,RP-500-0.3.0.7z) | [Bliz,RP-700](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz,RP-700-0.3.0.7z) |
| [Bliz,SC-300](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz,SC-300-0.3.0.7z) | [Bliz,SC-400](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz,SC-400-0.3.0.7z) | [Bliz,SC-500](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz,SC-500-0.3.0.7z) | [Bliz,SC-700](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-Bliz,SC-700-0.3.0.7z) |
| [PSimp-300](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-PSimp-300-0.3.0.7z) | [PSimp-400](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-PSimp-400-0.3.0.7z) | [PSimp-500](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-PSimp-500-0.3.0.7z) | [PSimp-700](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-PSimp-700-0.3.0.7z) |
| [PSimpChat-300](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-PSimpChat-300-0.3.0.7z) | [PSimpChat-400](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-PSimpChat-400-0.3.0.7z) | [PSimpChat-500](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-PSimpChat-500-0.3.0.7z) | [PSimpChat-700](https://gitee.com/nowar-fonts/Nowar-Rounded/raw/master/0.3.0/NowarRounded-PSimpChat-700-0.3.0.7z) |
